﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    //Punkteanzeige:
    public int score;
    public Text punkteanzeige;

    // Lebensanzeige:
    public int health;
    public int maxHealth;
    public RectTransform healthBalken;

    public float leben;
    public Text lebensanzeige;

    //Verschiedene Screens:
    public GameObject normalScreen;



    void Awake()    {
        if (instance == null){
            instance = this;
        }
        else if (instance != this){
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }


    void OnGUI()    {

        //Lebensanzeige:
        healthBalken.localScale = new Vector3((float)health / maxHealth, 1, 1);
        lebensanzeige.text = "Leben: " + leben.ToString();

        //Punkteanzeige:
        punkteanzeige.text = "Score: " + score.ToString();
    }

    public void Sterben()
    {
        Debug.Log("Du bist gestorben!");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        health = maxHealth;
        leben -= 1;
    }



    // Use this for initialization
    void Start () {
        health = maxHealth;
        leben = 3;
    }

    // Update is called once per frame
    void Update()
    {

        if (leben <= 0)
        {
            leben = 3;
            normalScreen.SetActive(false);
            SceneManager.LoadScene(0);
        }

        if (health <= 0)
        {
            Sterben();
        }

        if (health > 100)
        {
            health = maxHealth;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            normalScreen.SetActive(false);
            SceneManager.LoadScene(0);

        }
    }
}
